package com.fanghuaiming.notify.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/****
 * @description: 网关启动类
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午6:13
 *
 */
@SpringBootApplication
public class NotifyGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotifyGatewayApplication.class,args);
    }
}
