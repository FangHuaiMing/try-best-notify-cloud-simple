package com.fanghuaiming.notify.common.model.common;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/****
 * @description: 账户变更信息
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/12/3 下午4:14
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NotifyAccountChange implements Serializable {

    /**
     * 账号
     */
    private String accountNo;

    /**
     * 变动金额
     */
    private double amount;

    /**
     * 事务号
     */
    private String transactionId;
}
