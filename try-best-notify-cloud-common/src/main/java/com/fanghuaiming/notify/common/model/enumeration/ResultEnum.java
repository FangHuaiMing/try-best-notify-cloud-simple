package com.fanghuaiming.notify.common.model.enumeration;

/****
 * @description: 返回值枚举
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午6:03
 *
 */
public enum ResultEnum {

    SUCCESS("成功"),
    ERROR("失败"),
    FALL_BACK("熔断降级触发");

    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    ResultEnum(String message) {
        this.message = message;
    }
}
