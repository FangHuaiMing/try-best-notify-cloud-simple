package com.fanghuaiming.notify.common.model.enumeration;

/****
 * @description:
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/12/1 下午4:48
 *
 */
public enum PayTransactionEnum {

    /**
     * 成功
     */
    SUCCESS(1, "success"),
    /**
     * 异常
     */
    EXCEPTION(2, "exception"),

    /**
     * 未知
     */
    UNKOWN(3, "unkown");

    /**
     * 状态
     */
    Integer code;

    /**
     * 状态信息
     */
    String message;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    PayTransactionEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
