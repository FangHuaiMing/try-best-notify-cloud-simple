/*
 Navicat Premium Data Transfer

 Source Server         : localhost3307
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost:3307
 Source Schema         : notify_pay

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 03/12/2020 21:00:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for notify_pay
-- ----------------------------
DROP TABLE IF EXISTS `notify_pay`;
CREATE TABLE `notify_pay` (
  `transaction_id` varchar(64) COLLATE utf8_bin NOT NULL,
  `account_no` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '账号',
  `pay_amount` double DEFAULT NULL COMMENT '充值金额',
  `result` varchar(20) COLLATE utf8_bin DEFAULT 'success' COMMENT '充值结果:success,fail',
  PRIMARY KEY (`transaction_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of notify_pay
-- ----------------------------
BEGIN;
INSERT INTO `notify_pay` VALUES ('0fb33135-bafc-41a6-b2aa-ee10fb1e442b', '1', 500, NULL);
INSERT INTO `notify_pay` VALUES ('e671b3f3-adc2-460b-891d-eb964659149d', '1', 500, NULL);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
