/*
 Navicat Premium Data Transfer

 Source Server         : localhost3306
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost:3306
 Source Schema         : notify_account

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 03/12/2020 21:00:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for notify_account_duplication
-- ----------------------------
DROP TABLE IF EXISTS `notify_account_duplication`;
CREATE TABLE `notify_account_duplication` (
  `transaction_id` varchar(64) COLLATE utf8_bin NOT NULL,
  `create_time` datetime DEFAULT NULL,
  PRIMARY KEY (`transaction_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of notify_account_duplication
-- ----------------------------
BEGIN;
INSERT INTO `notify_account_duplication` VALUES ('0fb33135-bafc-41a6-b2aa-ee10fb1e442b', '2020-12-03 20:57:00');
INSERT INTO `notify_account_duplication` VALUES ('78fc0a3e-5065-450a-beca-b63463447d37', '2020-12-03 20:28:40');
INSERT INTO `notify_account_duplication` VALUES ('95e3ae79-f29d-4d48-8347-781266d5ed71', '2020-12-03 19:59:49');
INSERT INTO `notify_account_duplication` VALUES ('9fdfd392-6722-4672-9c55-c86f8bb1a733', '2020-12-03 19:58:55');
INSERT INTO `notify_account_duplication` VALUES ('ae0e448c-3317-4399-9398-c5e1cd8d0c16', '2020-12-03 20:23:55');
INSERT INTO `notify_account_duplication` VALUES ('bdf0a077-6576-41a8-9779-fed42bf3aad8', '2020-12-03 19:55:27');
INSERT INTO `notify_account_duplication` VALUES ('e671b3f3-adc2-460b-891d-eb964659149d', '2020-12-03 20:57:00');
INSERT INTO `notify_account_duplication` VALUES ('fc5788f3-f93a-40b7-ad5b-91871af811ae', '2020-12-03 20:47:01');
COMMIT;

-- ----------------------------
-- Table structure for notify_account_info
-- ----------------------------
DROP TABLE IF EXISTS `notify_account_info`;
CREATE TABLE `notify_account_info` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account_name` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '户主姓名',
  `account_no` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '银行卡号',
  `account_password` varchar(100) COLLATE utf8_bin DEFAULT NULL COMMENT '帐户密码',
  `account_balance` double DEFAULT NULL COMMENT '帐户余额',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of notify_account_info
-- ----------------------------
BEGIN;
INSERT INTO `notify_account_info` VALUES (2, '张三', '1', NULL, 2500);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
