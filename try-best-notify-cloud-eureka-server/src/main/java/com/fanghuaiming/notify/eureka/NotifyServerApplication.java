package com.fanghuaiming.notify.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/****
 * @description: eureka服务管理
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午3:49
 *
 */
@SpringBootApplication
@EnableEurekaServer
public class NotifyServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotifyServerApplication.class,args);
    }
}
