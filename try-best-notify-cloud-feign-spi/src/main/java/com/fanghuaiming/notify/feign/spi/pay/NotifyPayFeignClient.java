package com.fanghuaiming.notify.feign.spi.pay;

import org.springframework.cloud.openfeign.FeignClient;

/****
 * @description: 账户微服务SPI
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午5:19
 *
 */
@FeignClient(name = "try-best-notify-cloud-pay")
public interface NotifyPayFeignClient {


}
