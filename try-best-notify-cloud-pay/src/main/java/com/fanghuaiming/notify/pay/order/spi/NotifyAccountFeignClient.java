package com.fanghuaiming.notify.pay.order.spi;

import com.fanghuaiming.notify.pay.order.spi.fallback.NotifyAccountFeignClientFallBack;
import org.springframework.cloud.openfeign.FeignClient;

/****
 * @description: 账户微服务SPI
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午5:19
 *
 */
@FeignClient(name = "try-best-notify-cloud-account",fallback = NotifyAccountFeignClientFallBack.class)
public interface NotifyAccountFeignClient {

}
