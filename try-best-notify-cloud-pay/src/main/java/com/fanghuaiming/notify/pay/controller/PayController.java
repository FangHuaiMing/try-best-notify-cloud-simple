package com.fanghuaiming.notify.pay.controller;

import com.fanghuaiming.notify.pay.dao.model.NotifyPay;
import com.fanghuaiming.notify.pay.service.PayService;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/****
 * @description: 充值服务接口
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午2:00
 *
 */
@Slf4j
@RestController("pay")
@RequestMapping("/pay")
public class PayController {


    /**
     * 充值service
     */
    @Autowired
    private PayService payService;

    //充值
    /**
     * @Description: 进行充值
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午5:09
     * @version:1.0.0
     */
    @PostMapping(value = "/insertNotifyPay")
    public NotifyPay insertNotifyPay(@RequestBody NotifyPay notifyPay){
        //生成事务编号-可以使用雪花算法代替
        String txNo = UUID.randomUUID().toString();
        notifyPay.setTransactionId(txNo);
        return payService.insertNotifyPay(notifyPay);
    }

    /**
     * @Description: 查询充值结果
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午5:11
     * @version:1.0.0
     */
    @PostMapping(value = "/payresult")
    public NotifyPay payresult(@RequestParam("transactionId") String transactionId){
        return payService.getNotifyPayRequest(transactionId);
    }

}
