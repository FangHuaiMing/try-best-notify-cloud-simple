package com.fanghuaiming.notify.pay.service.impl;

import com.fanghuaiming.notify.common.model.enumeration.PayTransactionEnum;
import com.fanghuaiming.notify.pay.dao.mapper.NotifyPayMapper;
import com.fanghuaiming.notify.pay.dao.model.NotifyPay;
import com.fanghuaiming.notify.pay.service.PayService;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/****
 * @description: 充值服务接口实现类
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午1:59
 *
 */
@Service
@Slf4j
public class PayServiceImpl implements PayService {

    /**
     * 充值mapper
     */
    @Autowired
    private NotifyPayMapper notifyPayMapper;

    /**
     *  RocketMQ
     */
    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Value("${rocketmq.pay.destination}")
    private String destination;


    /**
     * @Description: 充值
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午5:12
     * @version:1.0.0
     */
    @SuppressWarnings("all")
    @Transactional
    @Override
    public NotifyPay insertNotifyPay(NotifyPay notifyPay) {
        log.info("====== 准备进行充值 ======");
        //进行充值
        int result = notifyPayMapper.insertNotifyPay(notifyPay);
        if(result > 0 ){
            // 发送普通消息进行通知
            log.info("====== 充值完成准备进行发送消息进行最大努力通知 ======");
            notifyPay.setResult(PayTransactionEnum.SUCCESS.getMessage());
            rocketMQTemplate.convertAndSend(destination,notifyPay);
            log.info("====== 最大努力通知消息发送完成 ======");
            log.info("====== 充值完成 ======");
            return notifyPay;
        }
        log.info("====== 充值失败 ======");
        return null;
    }

    /**
     * @Description: 查询充值结果
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午5:12
     * @version:1.0.0
     */
    @Override
    public NotifyPay getNotifyPayRequest(String transactionId) {
        log.info("====== 充值微服务 查询充值结果 ======");
        NotifyPay notifyPay = notifyPayMapper.findByTransactionId(transactionId);
        log.info("====== 充值微服务 查询充值成功:{} ======",notifyPay);
        return notifyPay;
    }
}
