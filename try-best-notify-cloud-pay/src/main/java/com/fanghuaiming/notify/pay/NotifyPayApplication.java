package com.fanghuaiming.notify.pay;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/****
 * @description: 充值微服务启动类
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 上午11:30
 *
 */
@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = "com.fanghuaiming.notify.pay")
@MapperScan(value = {"com.fanghuaiming.notify.pay.dao.mapper"})
@EnableFeignClients
public class NotifyPayApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotifyPayApplication.class,args);
    }
}
