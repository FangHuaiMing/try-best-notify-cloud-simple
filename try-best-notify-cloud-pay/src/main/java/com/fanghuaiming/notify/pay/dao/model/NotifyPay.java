package com.fanghuaiming.notify.pay.dao.model;

import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/****
 * @description: 支付表模型表
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 上午11:16
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NotifyPay implements Serializable {

    /**
     *  事务号
     */
    private String transactionId;

    /**
     * 账号
     */
    private String accountNo;

    /**
     * 变动金额
     */
    private double payAmount;

    /**
     * 充值结果
     */
    private String result;

}
