package com.fanghuaiming.notify.pay.order.spi.fallback;

import com.fanghuaiming.notify.pay.order.spi.NotifyAccountFeignClient;
import org.springframework.stereotype.Component;

/****
 * @description: 账户微服务熔断降级
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午6:01
 *
 */
@Component
public class NotifyAccountFeignClientFallBack implements NotifyAccountFeignClient {

}
