package com.fanghuaiming.notify.pay.service;

import com.fanghuaiming.notify.pay.dao.model.NotifyPay;

/****
 * @description: 充值服务接口
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午1:58
 *
 */
public interface PayService {

    /**
     * @Description: 充值
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午5:11
     * @version:1.0.0
     */
    NotifyPay insertNotifyPay(NotifyPay notifyPay);

    /**
     * @Description: 查询充值结果
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午5:11
     * @version:1.0.0
     */
    NotifyPay getNotifyPayRequest(String transactionId);
}
