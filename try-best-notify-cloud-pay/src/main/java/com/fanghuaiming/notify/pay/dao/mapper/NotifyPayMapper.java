package com.fanghuaiming.notify.pay.dao.mapper;

import com.fanghuaiming.notify.pay.dao.model.NotifyPay;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/****
 * @description: 充值dao操作接口
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午1:57
 *
 */
@Repository
public interface NotifyPayMapper {

    /**
     * @Description: 进行充值
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午5:05
     * @version:1.0.0
     */
    int insertNotifyPay(NotifyPay notifyPay);

    /**
     * @Description:
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午5:06
     * @version:1.0.0
     */
    NotifyPay findByTransactionId(@Param("transactionId") String transactionId);
}
