package com.fanghuaiming.notify.account;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/****
 * @description: 订单微服务启动类
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午2:38
 *
 */
@EnableTransactionManagement
@SpringBootApplication(scanBasePackages = "com.fanghuaiming.notify.account")
@MapperScan(value = {"com.fanghuaiming.notify.account.dao.mapper"})
@EnableFeignClients
@EnableScheduling
public class NotifyAccountApplication {

    public static void main(String[] args) {
        SpringApplication.run(NotifyAccountApplication.class,args);
    }
}
