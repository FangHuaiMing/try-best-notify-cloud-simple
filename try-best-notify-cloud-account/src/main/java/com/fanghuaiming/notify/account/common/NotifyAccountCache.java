package com.fanghuaiming.notify.account.common;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/****
 * @description: 定义项目缓存
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/12/3 下午5:53
 *
 */
public class NotifyAccountCache {

    /**
     * 多线程场景下读的并发高,写的并发没有那么高
     */
    public static CopyOnWriteArrayList<String> transactionIdList = new CopyOnWriteArrayList<>();

    /**
     * 定时处理超过2次的异常事务全局缓存
     */
    public static Map<String, AtomicInteger> transactionIdMap = new ConcurrentHashMap<>();
}
