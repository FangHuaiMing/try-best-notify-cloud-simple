package com.fanghuaiming.notify.account.controller;

import com.fanghuaiming.notify.account.service.NotityAccountInfoService;
import com.fanghuaiming.notify.common.model.pay.NotifyPay;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/****
 * @description: 账户服务接口
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午2:00
 *
 */
@RestController("account")
@RequestMapping("/account")
@Slf4j
public class AccountController {

    /**
     * 账户服务
     */
    @Autowired
    private NotityAccountInfoService notityAccountInfoService;

    /**
     * @Description: 主动查询充值结果
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午4:56
     * @version:1.0.0
     */
    @GetMapping(value = "/queryPayResult")
    public NotifyPay queryPayResult(@RequestParam("transactionId") String transactionId){
        NotifyPay notifyPay = notityAccountInfoService.queryPayResult(transactionId);
        return notifyPay;
    }

}
