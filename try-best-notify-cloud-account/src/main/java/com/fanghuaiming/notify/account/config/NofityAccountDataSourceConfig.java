package com.fanghuaiming.notify.account.config;

import com.zaxxer.hikari.HikariDataSource;
import javax.sql.DataSource;
import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

/****
 * @description:
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/28 下午8:44
 *
 */
@Configuration
@MapperScan(basePackages = NofityAccountDataSourceConfig.PACKAGE, sqlSessionFactoryRef = "notifyAccountSqlSessionFactory")
public class NofityAccountDataSourceConfig {

    static final String PACKAGE = "com.fanghuaiming.notify.account.dao.mapper";


    /**
     * @Description: 定义dataSource
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/1 下午1:30
     * @version:1.0.0
     */
    @Bean(name = "notifyAccountDataSource")
    @Primary
    @ConfigurationProperties(prefix = "spring.datasource.notify.account")
    public HikariDataSource notifyAccountDataSource() {
        return new HikariDataSource();
    }

    /**
     * @Description: 定义TransactionManager
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/1 下午1:31
     * @version:1.0.0
     */
    @Bean(name = "notifyAccountTransactionManager")
    @Primary
    public DataSourceTransactionManager notifyAccountTransactionManager() {
        return new DataSourceTransactionManager(this.notifyAccountDataSource());
    }

    /**
     * @Description: 定义SqlSessionFactory
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/1 下午1:31
     * @version:1.0.0
     */
    @Bean(name = "notifyAccountSqlSessionFactory")
    @Primary
    public SqlSessionFactory notifyAccountSqlSessionFactory(@Qualifier("notifyAccountDataSource") DataSource dataSource) throws Exception {
        final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setDataSource(dataSource);
        sessionFactory.setMapperLocations(
                new PathMatchingResourcePatternResolver().getResources("classpath:mappers/*.xml"));
        sessionFactory.getObject().getConfiguration().setMapUnderscoreToCamelCase(true);
        return sessionFactory.getObject();
    }

}
