package com.fanghuaiming.notify.account.service;

import com.fanghuaiming.notify.common.model.common.NotifyAccountChange;
import com.fanghuaiming.notify.common.model.pay.NotifyPay;

/****
 * @description: 账户服务接口
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午1:58
 *
 */
public interface NotityAccountInfoService {

    /**
     * @Description: 更新账户金额
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午4:06
     * @version:1.0.0
     */
    void updateNotifyAccountBalance(NotifyAccountChange notifyAccountChange);

    /**
     * @Description: 查询充值结果(远程调用)
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午4:06
     * @version:1.0.0
     */
    NotifyPay queryPayResult(String txNo);

    /**
     * @Description: 处理失败事务
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午6:47
     * @version:1.0.0
     */
    void dealErrorTransactionAccountBalance(NotifyAccountChange notifyAccountChange) ;
}
