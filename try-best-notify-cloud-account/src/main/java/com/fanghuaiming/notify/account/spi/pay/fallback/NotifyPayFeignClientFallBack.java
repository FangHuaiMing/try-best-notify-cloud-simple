package com.fanghuaiming.notify.account.spi.pay.fallback;

import com.fanghuaiming.notify.account.spi.pay.NotifyPayFeignClient;
import com.fanghuaiming.notify.common.model.enumeration.ResultEnum;
import com.fanghuaiming.notify.common.model.pay.NotifyPay;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/****
 * @description: 充值微服务熔断降级
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午6:01
 *
 */
@Component
@Slf4j
public class NotifyPayFeignClientFallBack implements NotifyPayFeignClient {

    @Override
    public NotifyPay payresult(String transactionId) {
    log.error(ResultEnum.FALL_BACK.getMessage());
        return null;
    }
}
