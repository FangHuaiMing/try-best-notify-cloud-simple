package com.fanghuaiming.notify.account.service.impl;

import com.fanghuaiming.notify.account.dao.mapper.NotifyAccountInfoMapper;
import com.fanghuaiming.notify.account.dao.model.NotifyAccountInfo;
import com.fanghuaiming.notify.account.service.NotityAccountInfoService;
import com.fanghuaiming.notify.account.spi.pay.NotifyPayFeignClient;
import com.fanghuaiming.notify.common.model.common.NotifyAccountChange;
import com.fanghuaiming.notify.common.model.pay.NotifyPay;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/****
 * @description: 充值服务接口实现类
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午1:59
 *
 */
@Service
@Slf4j
public class NotityAccountInfoServiceImpl implements NotityAccountInfoService {

    /**
     * 账户dao
     */
    @Autowired
    private NotifyAccountInfoMapper notifyAccountInfoMapper;

    /**
     * 充值微服务spi
     */
    @SuppressWarnings("all")
    @Autowired
    private NotifyPayFeignClient notifyPayFeignClient;

    /**
     * @Description: 更新账户金额
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午4:07
     * @version:1.0.0
     */
    @SuppressWarnings("all")
    @Override
    @Transactional
    public void updateNotifyAccountBalance(NotifyAccountChange notifyAccountChange) {
        //幂等校验
        if(notifyAccountInfoMapper.isExistTx(notifyAccountChange.getTransactionId()) > 0){
            log.info("======处理正常业务 幂等性校验 更新账户余额时发现该操作已经执行,无需重复执行 ======");
            return ;
        }
        //模拟异常
        if(notifyAccountChange.getAmount() == 500){
            log.info("======处理异常业务 本地事务失败进行回滚 做最大努力通知事务交由定时失败事务处理器处理 ======");
            int i = 1 / 0;
        }
        //进行实际操作
        int result = notifyAccountInfoMapper.updateNotifyAccountBalance(
                NotifyAccountInfo.builder().accountNo(notifyAccountChange.getAccountNo())
                        .accountBalance(notifyAccountChange.getAmount()).build());
        //插入事务记录，用于幂等控制
        notifyAccountInfoMapper.addTx(notifyAccountChange.getTransactionId());
    }

    /**
     * @Description: 处理失败事务
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午6:48
     * @version:1.0.0
     */
    @SuppressWarnings("all")
    @Override
    @Transactional
    public void dealErrorTransactionAccountBalance(NotifyAccountChange notifyAccountChange) {
        //幂等校验
        if(notifyAccountInfoMapper.isExistTx(notifyAccountChange.getTransactionId()) > 0){
            log.info("====== 处理失败事务 幂等性校验 更新账户余额时发现该操作已经执行,无需重复执行 ======");
            return ;
        }
        //进行实际操作
        int result = notifyAccountInfoMapper.updateNotifyAccountBalance(
                NotifyAccountInfo.builder().accountNo(notifyAccountChange.getAccountNo())
                        .accountBalance(notifyAccountChange.getAmount()).build());
        //插入事务记录，用于幂等控制
        notifyAccountInfoMapper.addTx(notifyAccountChange.getTransactionId());
    }

    /**
     * @Description: 查询充值结果(远程调用)
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午4:08
     * @version:1.0.0
     */
    @Override
    public NotifyPay queryPayResult(String txNo) {
        //查询充值微服务结果
        NotifyPay notifyPay = notifyPayFeignClient.payresult(txNo);
        return notifyPay;
    }
}
