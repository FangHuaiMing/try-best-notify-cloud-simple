package com.fanghuaiming.notify.account.dao.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/****
 * @description: 订单信息表
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 上午11:16
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class NotifyAccountInfo {

    /**
     * 主键id
     */
    private Long id;

    /**
     * 账户名
     */
    private String accountName;

    /**
     * 账户编号
     */
    private String accountNo;

    /**
     * 账户密码
     */
    private String accountPassword;

    /**
     * 账户余额/变更金额
     */
    private Double accountBalance;

}
