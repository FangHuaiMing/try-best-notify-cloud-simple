package com.fanghuaiming.notify.account.dao.mapper;

import com.fanghuaiming.notify.account.dao.model.NotifyAccountInfo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/****
 * @description: 账户dao操作接口
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午1:57
 *
 */
@Repository
public interface NotifyAccountInfoMapper {

    /**
     * @Description: 修改账户金额
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午4:01
     * @version:1.0.0
     */
    int updateNotifyAccountBalance(NotifyAccountInfo notifyAccountInfo) ;


    /**
     * @Description: 查询幂等记录，用于幂等控制
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午4:02
     * @version:1.0.0
     */
    int isExistTx(@Param("transactionId") String transactionId);

    /**
     * @Description: 添加事务记录，用于幂等控制
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午4:02
     * @version:1.0.0
     */
    int addTx(@Param("transactionId") String transactionId);

}
