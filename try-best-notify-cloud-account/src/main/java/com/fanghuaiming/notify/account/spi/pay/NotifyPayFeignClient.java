package com.fanghuaiming.notify.account.spi.pay;

import com.fanghuaiming.notify.account.spi.pay.fallback.NotifyPayFeignClientFallBack;
import com.fanghuaiming.notify.common.model.pay.NotifyPay;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/****
 * @description: 充值微服务SPI
 * @version:1.0.0
 * @author fanghuaiming
 * @data Created in 2020/11/27 下午5:19
 *
 */
@FeignClient(name = "try-best-notify-cloud-pay",fallback = NotifyPayFeignClientFallBack.class)
public interface NotifyPayFeignClient {

    /**
     * @Description: 查询充值结果
     *
     * @param:
     * @return:
     * @auther: fanghuaiming
     * @date: 2020/12/3 下午4:51
     * @version:1.0.0
     */
    @PostMapping("/pay/payresult")
    NotifyPay payresult(@RequestParam("transactionId") String transactionId);
}
